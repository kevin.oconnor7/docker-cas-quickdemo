FROM apereo/cas:v5.1.3

COPY etc/cas/config/ /cas-overlay/etc/cas/config/
COPY etc/cas/services/ /cas-overlay/etc/cas/services/
COPY pom.xml /cas-overlay/pom.xml
